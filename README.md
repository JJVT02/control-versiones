# CONTROL DE VERSIONES

![images](img/control.jpg)

[1. Introduccion](modulos/introduccion.md)

[2. Sistemas de control de versiones: Git](modulos/sistemas-control.md)

[3. Lenguaje de marcas: Markdown](modulos/lenguaje-marca.md)

[4. Repositorio: Github](modulos/repositorio.md)

[5. Conceptos basicos git/github](modulos/conceptos.md)

## Referencias
